A podcast loading cli for my Pokky project

* Free software: MIT license

## Setup

```bash
    virtualenv -p /usr/bin/python3 virtual_env
    source virtual_env/bin/activate
    python setup.py develop
```
