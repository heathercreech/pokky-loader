from sqlalchemy import engine_from_config
from sqlalchemy.ext.declarative import declarative_base, DeferredReflection
from sqlalchemy.orm import scoped_session, sessionmaker


session = scoped_session(sessionmaker())
Base = declarative_base(cls=DeferredReflection)


def init_sql(settings):

    engine = engine_from_config(settings)
    session.configure(bind=engine)

    Base.metadata.bind = engine
    Base.metadata.create_all(engine)
    Base.prepare(engine)
