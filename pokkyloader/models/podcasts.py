from sqlalchemy import Column, Integer

from pokkyloader.models.base import Base, session


class PodcastsBase(Base):
    __abstract__ = True
    __table_args__ = {'schema': 'podcasts'}

    @classmethod
    def query(cls, *args):
        if len(args) > 0:
            return session.query(*args)
        return session.query(cls)

    @classmethod
    def all(cls):
        return session.query(cls).all()

    @classmethod
    def get(cls, id):
        q = session.query(cls)
        q = q.filter(cls.id == id)
        return q.one_or_none()

    @classmethod
    def add(cls, records):
        new_records = []
        for record in records:
            new_record = cls()
            for key, value in record.items():
                setattr(new_record, key, value)
            session.merge(new_record)
            new_records.append(new_record)
        session.commit()
        return new_records


class Podcast(PodcastsBase):
    __tablename__ = 'podcast'
    # id = Column('id', Integer, primary_key=True)

    @classmethod
    def exists(cls, id):
        q = session.query(cls)
        q = q.filter(cls.id == id)
        return q.exists().scalar()

    @classmethod
    def search(cls, search_string):
        q = session.query(cls)
        q = q.filter(cls.title.ilike('%{0}%'.format(search_string)))
        q = q.order_by(cls.title)
        return q.all()

    @classmethod
    def get(cls, id):
        q = session.query(cls)
        q = q.filter(cls.id == id)
        return q.one_or_none()

    @classmethod
    def get_by_provider(cls, provider, provider_id):
        q = session.query(cls)
        q = q.filter(cls.provider == provider)
        q = q.filter(cls.provider_id == provider_id)
        return q.one_or_none()

    @classmethod
    def add(cls, records):
        new_records = []
        for record in records:
            new_record = None
            if 'provider' and 'provider_id' in record:
                new_record = cls.get_by_provider(
                    record['provider'],
                    record['provider_id']
                )
            if new_record is None:
                new_record = cls()
            for key, value in record.items():
                setattr(new_record, key, value)
            session.merge(new_record)
            new_records.append(new_record)
        session.commit()
        return new_records


class Episode(PodcastsBase):
    __tablename__ = 'episode'

    @classmethod
    def get(cls, podcast):
        q = session.query(cls)
        q = q.filter(cls.podcast == podcast)
        return q.all(), q.count()


class Language(PodcastsBase):
    __tablename__ = 'language'


class Provider(PodcastsBase):
    __tablename__ = 'provider'

    @classmethod
    def get_by_key(cls, key):
        q = session.query(cls.id)
        q = q.filter(cls.key == key)
        return q.one()
