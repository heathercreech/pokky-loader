# -*- coding: utf-8 -*-

"""Top-level package for pokky-loader."""

__author__ = """Heather Creech"""
__email__ = 'heatherannecreech@gmail.com'
__version__ = '0.1.0'
