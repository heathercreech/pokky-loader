import sys
import time
from configparser import ConfigParser
import click


from pokkyloader.lib.providers import ItunesProvider
from pokkyloader.models.base import init_sql


@click.command()
def main(args=None):
    config = ConfigParser()
    config.read('development.ini')
    init_sql(config._sections['app:main'])

    with open('/home/heather/data/words.txt', 'r') as f:
        lines = f.readlines()
        keep = [line for line in lines if len(line) > 4]
        with click.progressbar(lines) as bar:
            for line in bar:
                try:
                    ItunesProvider.search(search_string=line)
                except:
                    continue
                time.sleep(1)
    return 0


if __name__ == "__main__":
    sys.exit(main())  # pragma: no cover
